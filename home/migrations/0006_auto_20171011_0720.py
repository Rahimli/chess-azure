# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-10-11 03:20
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20171011_0638'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='publish_end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='blog',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 11, 7, 20, 54, 509652)),
        ),
        migrations.AlterField(
            model_name='tournament',
            name='publish_end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='tournament',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 11, 7, 20, 54, 515649)),
        ),
    ]
