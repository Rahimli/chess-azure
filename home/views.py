from django.core.paginator import EmptyPage, PageNotAnInteger
from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response, get_object_or_404
from flynsarmy_paginator import FlynsarmyPaginator

from django.utils.translation import ugettext_lazy as _
from .forms import *
from django.utils import timezone

# Create your views here.


# @login_required(login_url='login')
# from django.template import RequestContext
from django.template import RequestContext

from home.models import *


def home(request):
    slider = Slider.objects.filter(active=True).order_by('-date').first()
    last_blogs = Blog.objects.filter(active=True).order_by('publish_start_date')[:3]
    tournaments = Tournament.objects.filter(active=True).order_by('publish_start_date')[:6]
    page_index = 'home'
    context = {
        'slider': slider,
        'last_blogs': last_blogs,
        'page_index': page_index,
        'tournaments': tournaments
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/index.html', context, context_instance=RequestContext(request))


def apply(request):
    apply_form = ApplyForm(request.POST or None)
    slider = Slider.objects.filter(active=True).order_by('-date').first()
    socials = Social.objects.all().order_by('order_index')
    context = {
        'slider': slider,
        'base_socials': socials,
    }
    if request.method == 'POST' and apply_form.is_valid():
        clean_data = apply_form.cleaned_data
        first_name = clean_data.get('first_name')
        last_name = clean_data.get('last_name')
        phone = clean_data.get('phone')
        birth_date = clean_data.get('birth_date')
        message = clean_data.get('message')
        apply_o = Apply(first_name=first_name, last_name=last_name, phone=phone, birth_date=birth_date, message=message)
        apply_o.save()
        post_message_code = 1
        post_message = _("Your request was successfully sent")
        apply_form = ApplyForm()
        context.update({
            'apply_form': apply_form,
            'post_message_code': post_message_code,
            'post_message': post_message,
        })
    context.update({
        'apply_form': apply_form,
    })
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/apply.html', context, context_instance=RequestContext(request))


def blog(request):
    categories = Category.objects.filter(active=True).order_by('order_index')
    last_tournaments = Tournament.objects.filter(active=True).order_by('publish_start_date')[:6]
    blog_list = Blog.objects.filter(active=True).order_by('-publish_start_date')

    paginator = FlynsarmyPaginator(blog_list, 25, adjacent_pages=3)  # Show 25 contacts per page

    page = request.GET.get('page')  # page check is tam eded
    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        blogs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        blogs = paginator.page(paginator.num_pages)

    context = {
        'blogs': blogs,
        'categories': categories,
        'last_tournaments': last_tournaments,
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/blog.html', context, context_instance=RequestContext(request))


def news(request):
    now = timezone.now()
    tournament_list = Tournament.objects.filter(active=True,tournament_date__gte=now).order_by('-publish_start_date')

    paginator = FlynsarmyPaginator(tournament_list, 25, adjacent_pages=3)  # Show 25 contacts per page

    last_blogs = Blog.objects.filter(active=True).order_by('publish_start_date')[:6]
    page = request.GET.get('page')  # page check is tam eded
    try:
        tournaments = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tournaments = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tournaments = paginator.page(paginator.num_pages)

    context = {
        'tournaments': tournaments,
        'last_blogs': last_blogs,
        # 'blogs': blogs,
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/news.html', context, context_instance=RequestContext(request))


def blog_category(request, c_id):
    category = get_object_or_404(Category, id=c_id)
    categories = Category.objects.filter(active=True).order_by('order_index')
    last_tournaments = Tournament.objects.filter(active=True).order_by('publish_start_date')[:6]
    blog_list = Blog.objects.filter(active=True).filter(category=category).order_by('-publish_start_date')
    paginator = FlynsarmyPaginator(blog_list, 25, adjacent_pages=3)  # Show 25 contacts per page

    page = request.GET.get('page')  # page check is tam eded
    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        blogs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        blogs = paginator.page(paginator.num_pages)

    context = {
        'category': category,
        'categories': categories,
        'blogs': blogs,
        'last_tournaments': last_tournaments,
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/blog.html', context, context_instance=RequestContext(request))


def tournaments(request):
    now = timezone.now()
    tournament_list = Tournament.objects.filter(active=True,tournament_date__lt=now).order_by('-publish_start_date')
    paginator = FlynsarmyPaginator(tournament_list, 25, adjacent_pages=3)  # Show 25 contacts per page

    last_blogs = Blog.objects.filter(active=True).order_by('publish_start_date')[:6]
    page = request.GET.get('page')  # page check is tam eded
    try:
        tournaments = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        tournaments = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        tournaments = paginator.page(paginator.num_pages)

    context = {
        'tournaments': tournaments,
        'last_blogs': last_blogs,
        # 'blogs': blogs,
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/tournaments.html', context, context_instance=RequestContext(request))


def blog_news(request, n_id, n_slug):
    news = get_object_or_404(Blog, id=n_id, slug=n_slug)
    categories = Category.objects.filter(active=True).order_by('order_index')
    last_blogs = Blog.objects.filter(active=True).order_by('publish_start_date')[:6]
    context = {
        'news': news,
        'categories': categories,
        'last_blogs': last_blogs
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/blog_news.html', context, context_instance=RequestContext(request))


def tournament(request, n_id, n_slug):
    tournament = get_object_or_404(Tournament, id=n_id, slug=n_slug)
    last_tournaments = Tournament.objects.filter(active=True).order_by('publish_start_date')[:6]
    context = {
        'tournament': tournament,
        'last_tournaments': last_tournaments,
        # 'apply_form': apply_form
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/tournament.html', context, context_instance=RequestContext(request))


def portfolio(request):
    user = request.user
    slider = Slider.objects.filter(active=True).order_by('-date').first()
    context = {
        'slider': slider,
        # 'last_news':last_news
    }
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/index.html', context, context_instance=RequestContext(request))


def about_vuqar(request, v_slug):
    user = request.user
    vugar_page = VugarPage.objects.filter(active=True).first()
    context = {
        'vugar_page': vugar_page,
        'v_slug': v_slug,
    }
    if (v_slug == 'biography'):
        title = _('biography')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    elif (v_slug == 'childhood'):
        title = _('vuqar_childhood')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    elif (v_slug == 'titles'):
        title = _('vuqar_titles')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    # elif (v_slug == 'participan'):
    #     title = _('participan_tourments')
    #     context.update({
    #         'title': title,
    #         # 'last_news':last_news
    #     })
    elif (v_slug == 'photos'):
        title = _('my_vugars_world')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    elif (v_slug == 'aphorism'):
        title = _('aphorism')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    elif (v_slug == 'vuqar-in-the-world'):
        title = _('vuqar-in-the-world')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    elif (v_slug == 'genius'):
        title = _('genius')
        context.update({
            'title': title,
            # 'last_news':last_news
        })
    elif (v_slug == 'disinterested-human'):
        title = _('disinterested-human')
        context.update({
            'title': title,
        })
    elif (v_slug == 'vugar-museum'):
        title = _('vugar-museum')
        context.update({
            'title': title,
        })
    else:
        # return HttpResponse('Salam')
        raise Http404
    # return render(request, 'home/index.html', context, content_type='application/xhtml+xml')
    return render_to_response('home/about_vugar.html', context, context_instance=RequestContext(request))
