import hashlib
import random
import re

from django.utils.translation import ugettext as _
import datetime

from django.utils import formats
from django.contrib.auth.models import Group as A_Group
from django.contrib import auth
from django.contrib.auth import authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext

from chess import settings

# Create your views here.
from content.models import *
from home.models import Social
from userprofile.forms import *


def context_processor(request):
    now = timezone.now()
    base_socials = Social.objects.all().order_by('order_index')
    context = {'base_socials':base_socials}

    if request.user.is_authenticated():
        user = request.user
        base_teacher = Teacher.objects.filter(user=user).first()
        base_student = Student.objects.filter(user=user).first()
        if base_teacher:
            profile = base_teacher
            type = 'teacher'
        elif base_student:
            profile = base_student
            type = 'student'
        else:
            profile = None
            type = ''
        context.update({
            'base_profile': profile,
            'base_type': type
        })
    return context


# --------------------------------------------------------------------------
@login_required(login_url='login')
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/login/')


def login(request):
    logout(request)
    next_url = request.GET.get("next_url")
    context = {
        'next_url': next_url
    }

    return render_to_response('userprofile/registration/login.html', context, context_instance=RequestContext(request))


def login_user(request):
    logout(request)
    username = password = ''
    next_url = '/profile/'
    if request.method == 'POST':
        # POST goes here . is_ajax is must to capture ajax requests. Beginners pit.
        if request.is_ajax():
            username = request.POST['access_name']
            password = request.POST['access_password']
            if '@' in username:
                try:
                    user_email = settings.AUTH_USER_MODEL.objects.get(email=username)
                    user = auth.authenticate(username=user_email.username,
                                             password=password)
                except:
                    user = auth.authenticate(username=username,
                                             password=password)
            else:
                user = auth.authenticate(username=username,
                                         password=password)
            if user:
                if user.is_active:
                    auth.login(request, user)
                    message1 = "Successfully Login"
                    message_code = 0
                    next_url = request.GET.get("next_url")
                    if next_url == None:
                        print(next_url)
                        next_url = '/'
                    else:
                        next_url = request.GET.get("next_url")
                else:
                    message1 = _("login-activate-profile-message")
                    message_code = 1
            else:
                if '@' in username:
                    message1 = _('email_or_password_is_incorrect') + ' !'
                else:
                    message1 = _('username_or_password_is_incorrect') + ' !'

                message_code = 2
            data = {"message": message1, "message_code": message_code, 'next_url': next_url}
            return JsonResponse(data)
        else:
            raise Http404
    else:
        raise Http404


# -----------------------------------------------------------------------------

@login_required(login_url='login')
def profile(request):
    user = request.user
    student = Student.objects.filter(user=user).first()
    teacher = Teacher.objects.filter(user=user).first()
    # return HttpResponse(teacher.)
    context = {
        'student': student,
        'teacher': teacher,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/profile.html', context, context_instance=RequestContext(request))


@login_required(login_url='login')
def profile_student(request):
    user = request.user
    student = get_object_or_404(Student, user=user)

    context = {
        'student': student,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/student/degrees.html', context, context_instance=RequestContext(request))


@login_required(login_url='login')
def student_feedback(request):
    user = request.user
    student = get_object_or_404(Student, user=user)
    teacher = Teacher.objects.filter(user=user)
    if teacher:
        raise Http404

    feedbacks = Feedback.objects.filter(student=student)

    context = {
        'student': student,
        'feedbacks': feedbacks,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/student/feedbacks.html', context, context_instance=RequestContext(request))


@login_required(login_url='login')
def student_feedback_ajax(request):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        form_data = request.POST
        feedback_title = form_data['feedback_title']
        feedback_content = form_data['feedback_content']
        student = get_object_or_404(Student, user=user)
        if feedback_title and feedback_content:
            feedback = Feedback(title=feedback_title, student=student, content=feedback_content)
            feedback.save()
            date = str(feedback.date.strftime("%d %B %Y"))
            message = _('successfully_send_feedback')
            message_code = 1
            contnet = str(feedback.content)
        else:
            message = ('try again')
            message_code = 0
            date = ''
            contnet = ''
        data = {"message_code": message_code, "message": message, "contnet": contnet, "date": date}
        return JsonResponse(data)
    else:
        raise Http404


@login_required(login_url='login')
def student_search(request):
    user = request.user
    teacher = get_object_or_404(Teacher, user=user)
    # now = timezone.now()
    # tag = get_object_or_404(Tag,slug=t_slug)
    search = request.GET.get("search")
    if not search:

        context = {
            # 'search':search,
        }
        return render_to_response('userprofile/teacher/search.html',
                                  context,
                                  context_instance=RequestContext(request))
    else:
        data_list = Student.objects.filter(
            Q(user__first_name__icontains=search) | Q(user__last_name__icontains=search) | Q(
                times_code__icontains=search))
        data_count = data_list.count()
        # paginator = Paginator(data_list, 40)  # Show 25 contacts per page
        #
        # page = request.GET.get('page')
        # try:
        #     datas = paginator.page(page)
        # except PageNotAnInteger:
        #     # If page is not an integer, deliver first page.
        #     datas = paginator.page(1)
        # except EmptyPage:
        #     # If page is out of range (e.g. 9999), deliver last page of results.
        #     datas = paginator.page(paginator.num_pages)
        # return HttpResponse(articles.count(articles))
        context = {
            'search': search,
            'datas': data_list,
            'data_count': data_count,
        }
        return render_to_response('userprofile/teacher/search.html',
                                  context,
                                  context_instance=RequestContext(request))


@login_required(login_url='login')
def student_view(request, username):
    user = request.user
    teacher = get_object_or_404(Teacher, user=user)
    user_student = get_object_or_404(User, username=username)
    student = get_object_or_404(Student, user=user_student)

    context = {
        'student': student,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/teacher/student_view.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def profile_teacher(request, prof_type):
    user = request.user
    if prof_type == 'student':
        profile = get_object_or_404(Student, user=user)
        template = 'userprofile/student/profile.html'
    elif prof_type == 'teacher':
        profile = get_object_or_404(Teacher, user=user)
        template = 'userprofile/teacher/profile.html'
    else:
        raise Http404

    context = {
        'profile': profile,
        'prof_type': prof_type,
        # 'last_news':last_news
    }

    return render_to_response(template, context, context_instance=RequestContext(request))


@login_required(login_url='login')
def group_page(request, g_id):
    user = request.user
    teacher = get_object_or_404(Teacher, user=user)
    group = get_object_or_404(Group, id=g_id)
    journal = Journal.objects.filter(group=group).first()

    context = {
        'teacher': teacher,
        'group': group,
        'journal': journal,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/teacher/group.html', context, context_instance=RequestContext(request))




@login_required(login_url='login')
def my_group(request):
    user = request.user
    student = get_object_or_404(Student, user=user)
    group = Group.objects.filter(students=student).first()
    teacher_rate = TeacherRate.objects.filter(student=student).filter(teacher=group.teacher).first()
    if teacher_rate.rate:
       rate = teacher_rate.rate
       # return HttpResponse(rate)
    else:
        rate = 0
        # return HttpResponse(rate)

    if request.method == 'POST' and request.is_ajax():
        data_p = request.POST
        star = data_p.get('star')
        # star = form_data['star']
        # print(star)
        # print(star)
        # print(star)
        # print(star)
        # print(star)
        # print(star)
        # print(star)
        rate = TeacherRate.objects.filter(student=student).filter(teacher=group.teacher).first()
        # print('------------------------' + str(student.id))
        # print('------------------------' + str(group.teacher.id))
        if rate:
            rate.rate = star
            rate.save()
            message_code = 1
        else:
            pass
            rate = TeacherRate(student=student,teacher=group.teacher,rate=rate)
            rate.save()
            message_code = 1
        data = {"message_code": message_code,"rate":star}
        return JsonResponse(data)
    if not group:
        raise Http404
    # return HttpResponse(rate)
    context = {
        'student': student,
        'rate': rate,
        'group': group,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/student/group.html', context, context_instance=RequestContext(request))




@login_required(login_url='login')
def journals(request):
    user = request.user
    teacher = get_object_or_404(Teacher, user=user)
    journals = Journal.objects.filter(group__teacher=teacher)

    context = {
        'teacher': teacher,
        'journals': journals,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/teacher/journals.html', context, context_instance=RequestContext(request))


@login_required(login_url='login')
def journal_page(request, j_id, jp_number):
    user = request.user
    teacher = get_object_or_404(Teacher, user=user)
    journal = get_object_or_404(Journal, id=j_id)
    if journal.group.teacher == teacher:
        journal_page = get_object_or_404(JournalPage, journal=journal, ord=jp_number)
    else:
        raise Http404
    # return HttpResponse(journal)
    for d in range(1, 13):
        journal_page_date = JournalPageDate.objects.filter(journal_page=journal_page, day=d).first()
        if not journal_page_date:
            journal_page_date_obj = JournalPageDate(journal_page=journal_page, day=d)
            journal_page_date_obj.save()
        for g_student in journal.group.students.all():
            participant = Participant.objects.filter(journalpage=journal_page, day=d, student=g_student)
            if not participant:
                # raise Http404
                participant = Participant(journalpage=journal_page, day=d, student=g_student, is_here=False)
                participant.save()
    participants = Participant.objects.filter(journalpage=journal_page)
    journal_page_dates = JournalPageDate.objects.filter(journal_page=journal_page).order_by('day')
    lesson_range = range(1, 13)
    date_range = range(1, 13)
    # return HttpResponse(participants.count())
    context = {
        'teacher': teacher,
        'journal': journal,
        'journal_page': journal_page,
        'lesson_range': lesson_range,
        'date_range': date_range,
        'participants': participants,
        'journal_page_dates': journal_page_dates,
    }

    return render_to_response('userprofile/teacher/journal.html', context, context_instance=RequestContext(request))


@login_required(login_url='login')
def journal_ajax(request, j_id, jp_number):
    if request.method == 'POST' and request.is_ajax():
        user = request.user
        teacher = get_object_or_404(Teacher, user=user)
        journal = get_object_or_404(Journal, id=j_id)
        if journal.group.teacher == teacher:
            journal_page = get_object_or_404(JournalPage, journal=journal, ord=jp_number)
        else:
            raise Http404
        student_list = []
        for student in journal.group.students.all():
            student_list.append(student.id)
        data_p = request.POST
        #
        # day_0_list=data_p.getlist('checkbox_day_0')
        # day_1_list=data_p.getlist('checkbox_day_1')
        # day_2_list=data_p.getlist('checkbox_day_2')
        # day_3_list=data_p.getlist('checkbox_day_3')
        # day_4_list=data_p.getlist('checkbox_day_4')
        # day_5_list=data_p.getlist('checkbox_day_5')
        # day_6_list=data_p.getlist('checkbox_day_6')
        # day_7_list=data_p.getlist('checkbox_day_7')
        # day_8_list=data_p.getlist('checkbox_day_8')
        # day_9_list=data_p.getlist('checkbox_day_9')
        # day_10_list=data_p.getlist('checkbox_day_10')
        # day_11_list=data_p.getlist('checkbox_day_11')



        # days_list = [day_0_list,day_1_list,day_2_list,day_3_list,day_4_list,day_5_list,day_6_list,day_7_list,day_8_list,day_8_list,day_9_list,day_10_list,day_11_list]


        # date_1=data_p.get('date_1')
        # date_2=data_p.get('date_2')
        # date_3=data_p.get('date_3')
        # date_4=data_p.get('date_4')
        # date_5=data_p.get('date_5')
        # date_6=data_p.get('date_6')
        # date_7=data_p.get('date_7')
        # date_8=data_p.get('date_8')
        # date_9=data_p.get('date_9')
        # date_10=data_p.get('date_10')
        # date_11=data_p.get('date_11')
        # date_12=data_p.get('date_12')

        # date_list =[date_1,date_2,date_3,date_4,date_5,date_6,date_7,date_8,date_9,date_10,date_11,date_12]


        # date_day_zip = zip(day_list,date_list)

        # zip(student_list,data_p.get('datepicker_day_1'),data_p.get('checkbox_day_1'))

        # print(date_day_zip)
        # game_list = ['Puerto Rico', 'Eclipse', 'Settlers of Catan', 'Agricola']
        #
        # # the list that will hold the bulk insert
        # bulk_games = []
        # print(data_p.getlist('checkbox_day_2'))
        for d in range(1, 13):
            for g_student in journal.group.students.all():
                participant = Participant.objects.filter(journalpage=journal_page, day=d, student=g_student)
                if not participant:
                    participant = Participant(journalpage=journal_page, day=d, student=g_student, is_here=False)
                    participant.save()
                participant = Participant.objects.filter(journalpage=journal_page, day=d, student=g_student)

        for d in range(1, 13):
            check_datas = data_p.getlist('checkbox_day_' + str(d))
            # print(check_datas)
            i = 0
            journal_page_date = JournalPageDate.objects.filter(journal_page=journal_page, day=d).first()
            if not journal_page_date:
                journal_page_date_obj = JournalPageDate(journal_page=journal_page, day=d)
                journal_page_date_obj.save()
            # print(date)
            for student in data_p.getlist('student_id'):
                # print(i)
                # print(check_datas[i])
                # for day in data_p.getlist('checkbox_day_'+str(d)):
                # print('---------------------------------------------')
                # print(check_datas[i])
                # # print('---------------------------------------------')
                # for day in day_list:
                # print(day)
                print(student)
                print(journal_page.ord)
                student_o = Student.objects.filter(user__id=student).first()
                print(str(d) + '----------' + str(student_o.id) + '----------' + str(
                    student_o.user.username) + '----------' + '----------' + str(journal_page.pk))
                participanto = Participant.objects.filter(day=int(d), journalpage=journal_page,
                                                          student=student_o).first()
                # print(participanto.date)
                if participanto:
                    print('1111111111111111111111111111')
                else:
                    print('000000000000000000000000000000')
                if check_datas[i] == '1':
                    participanto.is_here = True
                else:
                    participanto.is_here = False
                date_str = data_p.get('date_' + str(d))
                if date_str:
                    # 23 / 02 / 2017
                    p_date = date_str.split('/', )
                    p_date = str(p_date[2]) + '-' + str(p_date[0]) + '-' + str(p_date[1])
                    # = "2014-04-07"
                    p_date = timezone.datetime.strptime(p_date, "%Y-%m-%d")
                    print(p_date.day)
                    participanto.date = p_date
                    journal_page_date.date = p_date
                    journal_page_date.save()

                participanto.save()
                i = i + 1
                # print('******************************************************')

        # loop that list and make those game objects
        # for g in game_list:
        #     new_game = Participant()
        #     new_game.title = g
        #
        # # add game to the bulk list
        # bulk_games.append(new_game)
        #
        # # now with a list of game objects that want to be created, run bulk_create on the chosen model
        # Game.objects.bulk_create(bulk_list)
        # # bulk_games = []
        #
        # #loop that list and make those game objects
        # for day1 in day1_list:
        #     participant = Participant()
        #     participant.title = g
        #
        # #add game to the bulk list
        # bulk_games.append(new_game)
        #
        # #now with a list of game objects that want to be created, run bulk_create on the chosen model
        # Game.objects.bulk_create(bulk_list)
        data = {'message': 'Successfuly Changed', 'message_code': 1}
        return JsonResponse(data=data)


# @login_required(login_url='login')
# def change_password(request):
#     # user = request.user
#     # user = get_object_or_404(User,username=user.username,is_active=True)
#     # profile = Profile.objects.get(user=user)
#     # my_rate_films = Film_Rating.objects.filter(profile=profile)
#     # my_news = Article.objects.filter(author=user).order_by('-create_date')
#
#     context = {
#         # 'my_newss':my_news,
#         # 'new_films':new_films,
#         # 'last_news':last_news
#     }
#
#     return render_to_response('userprofile/change_password.html', context, context_instance=RequestContext(request))
#
#
# @login_required(login_url='login')
# def change_profile_ajax(request):
#     user = request.user
#     if request.method == 'POST' and request.is_ajax():
#         form_data = request.POST
#         first_name = form_data['first_name']
#         last_name = form_data['last_name']
#         password = form_data['password']
#         core = authenticate(username=user.username, password=password)
#         if core:
#             user.first_name = first_name
#             user.last_name = last_name
#             user.save()
#             message = 'success'
#         else:
#             message = 'invalid' + user.password
#         data = {"message": message,}
#         return JsonResponse(data)
#     else:
#         raise Http404
#
#
# @login_required(login_url='login')
# def changePassword_ajax(request):
#     if request.method == 'POST' and request.is_ajax():
#         data_p = request.POST
#         current_password = data_p.get('change-current-password')
#         new_password = data_p.get('change-new-password')
#         repeat_password = data_p.get('change-repeat-password')
#         user = request.user
#         core = authenticate(username=user.username, password=current_password)
#         if core:
#             if str(new_password).strip() == str(repeat_password).strip() and len(str(new_password).strip()) > 7:
#                 if re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', str(new_password).strip()):
#                     user.set_password(new_password)
#                     res = 1
#                     message = 'Succesfully Changed Password'
#                 else:
#                     res = 0
#                     message = 'Passwords Not Format Passwords'
#             else:
#                 res = 0
#                 message = 'Passwords Not Format Passwords'
#         else:
#             res = 0
#             message = 'Password is Incorrect' + current_password + ' - ' + user.password
#         data = {"message": message, 'res': res}
#         return JsonResponse(data)
#     else:
#         raise Http404
#
#
# @login_required(login_url='login')
# def profiles_info_change(request):
#     user = request.user
#     user = get_object_or_404(User, username=user.username, is_active=True)
#     # profile = Profile.objects.get(user=user)
#     # my_rate_films = Film_Rating.objects.filter(profile=profile)
#     # my_news = Article.objects.filter(author=user).order_by('-create_date')
#
#     context = {
#         # 'my_newss':my_news,
#         # 'new_films':new_films,
#         # 'last_news':last_news
#     }
#
#     return render_to_response('userprofile/profiles_info_change.html', context,
#                               context_instance=RequestContext(request))


# datepicker_day_1 = data_p.get('datepicker_day_1')
# datepicker_day_2 = data_p.get('datepicker_day_2')
# datepicker_day_3 = data_p.get('datepicker_day_3')
# datepicker_day_4 = data_p.get('datepicker_day_4')
# datepicker_day_5 = data_p.get('datepicker_day_5')
# datepicker_day_6 = data_p.get('datepicker_day_6')
# datepicker_day_7 = data_p.get('datepicker_day_7')
# datepicker_day_8 = data_p.get('datepicker_day_8')
# datepicker_day_9 = data_p.get('datepicker_day_9')
# datepicker_day_10 = data_p.get('datepicker_day_10')
# datepicker_day_11 = data_p.get('datepicker_day_11')
# datepicker_day_12 = data_p.get('datepicker_day_12')
#
# checkbox_day_1 = data_p.get('checkbox_day_1')
# checkbox_day_2 = data_p.get('checkbox_day_2')
# checkbox_day_3 = data_p.get('checkbox_day_3')
# checkbox_day_4 = data_p.get('checkbox_day_4')
# checkbox_day_5 = data_p.get('checkbox_day_5')
# checkbox_day_6 = data_p.get('checkbox_day_6')
# checkbox_day_7 = data_p.get('checkbox_day_7')
# checkbox_day_8 = data_p.get('checkbox_day_8')
# checkbox_day_9 = data_p.get('checkbox_day_9')
# checkbox_day_10 = data_p.get('checkbox_day_10')
# checkbox_day_11 = data_p.get('checkbox_day_11')
# checkbox_day_12 = data_p.get('checkbox_day_12')


@login_required(login_url='login')
def studentlist(request):
    user = request.user
    if not user.is_superuser:
        raise Http404
    data_list = Student.objects.all()
    paginator = Paginator(data_list, 20)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        students = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        students = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        students = paginator.page(paginator.num_pages)
    context = {
        'students': students,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/registration/students.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def createstudent(request):
    # user = get_object_or_404(User,id=id)
    # student = get_object_or_404(Student,user=user)
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    form = StudentForm(request.POST or None, request.FILES or None)
    context = {
        'form': form
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            first_name = clean_data.get('first_name')
            last_name = clean_data.get('last_name')
            father_name = clean_data.get('father_name')
            passport_number = clean_data.get('passport_number')
            email = clean_data.get('email')
            image = clean_data.get('image')
            birth_date = clean_data.get('birth_date')
            started_date = clean_data.get('started_date')
            home_address = clean_data.get('home_address')
            phone = clean_data.get('phone')
            times_code = clean_data.get('times_code')
            school = clean_data.get('school')
            check = User.objects.all()
            if check.filter(username=passport_number):
                form.add_error('passport_number', passport_number + ' allready exists')
                return render_to_response('userprofile/registration/student_add_edit.html', context,
                                          context_instance=RequestContext(request))
            if check.filter(email=email):
                form.add_error('email', email + ' allready exists')
                return render_to_response('userprofile/registration/student_add_edit.html', context,
                                          context_instance=RequestContext(request))

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(phone, salt=salt)
            new_user = User(first_name=first_name, last_name=last_name, username=passport_number, email=email,
                            password=password, is_active=True, is_staff=True)
            new_user.save()

            student = Student(
                user=new_user,
                first_name=first_name,
                last_name=last_name,
                father_name=father_name,
                passport_number=passport_number,
                image=image,
                email=email,
                birth_date= str(started_date),
                started_date= str(birth_date),
                home_address=home_address,
                phone=phone,
                times_code=times_code,
                school=school,
            )

            student.save()
            return HttpResponseRedirect('/profile/list/student/?create-messages=' + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/student_add_edit.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def editstudent(request, s_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    # return HttpResponse(user_s.id)
    student = get_object_or_404(Student, user=user_s)
    # return HttpResponse('dsd')
    form = StudentForm(request.POST or None, request.FILES or None,
                       initial={
                           'first_name': student.first_name,
                           'last_name': student.last_name,
                           'father_name': student.father_name,
                           'passport_number': student.passport_number,
                           'email': student.email,
                           'birth_date': student.birth_date,
                           'started_date': student.started_date,
                           'home_address': student.home_address,
                           'phone': student.phone,
                           'times_code': student.times_code,
                           'school': student.school,
                       },
                       )
    context = {
        'form': form,
        'student': student,
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            first_name = clean_data.get('first_name')
            last_name = clean_data.get('last_name')
            father_name = clean_data.get('father_name')
            passport_number = clean_data.get('passport_number')
            email = clean_data.get('email')
            image = clean_data.get('image')
            birth_date = clean_data.get('birth_date')
            started_date = clean_data.get('started_date')
            home_address = clean_data.get('home_address')
            phone = clean_data.get('phone')
            times_code = clean_data.get('times_code')
            school = clean_data.get('school')
            check = User.objects.all().exclude(id=s_id)
            if check.filter(username=passport_number):
                form.add_error('passport_number', passport_number + ' allready exists')
                return render_to_response('userprofile/registration/student_add_edit.html', context,
                                          context_instance=RequestContext(request))
            if check.filter(email=email):
                form.add_error('email', email + ' allready exists')
                return render_to_response('userprofile/registration/student_add_edit.html', context,
                                          context_instance=RequestContext(request))

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(phone, salt=salt)
            # new_user = User(first_name=first_name, last_name=last_name, username=passport_number, email=email,
            #                 password=password, is_active=True,is_staff=True)
            # new_user.save()
            user_s.first_name = first_name
            user_s.last_name = last_name
            user_s.username = passport_number
            user_s.email = email
            user_s.password = password
            user_s.save()

            student.first_name = first_name
            student.last_name = last_name
            student.email = email
            student.father_name = father_name
            student.passport_number = passport_number
            student.birth_date = birth_date
            student.started_date = started_date
            if image:
                student.image = image
            student.phone = phone
            student.home_address = home_address
            student.times_code = times_code
            student.school = school
            # HttpResponse(student.first_name)
            olda_f = student.father_name
            student.save()
            # return HttpResponse(olda_f + '----------------------------'+student.father_name + '________________________' +father_name)
            # HttpResponse(student.first_name)
            # HttpResponse(first_name)
            # student = Student(
            #     first_name=first_name,
            #     last_name=last_name,
            #     father_name=father_name,
            #     passport_number=passport_number,
            #     email=email,
            #     birth_date=now,
            #     started_date=now,
            #     home_address=home_address,
            #     phone=phone,
            #     times_code=times_code,
            #     school=school,
            # )
            return HttpResponseRedirect('/profile/list/student/?create-messages=' + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/student_add_edit.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def teacherlist(request):
    user = request.user
    if not user.is_superuser:
        raise Http404
    data_list = Teacher.objects.all()
    paginator = Paginator(data_list, 20)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        teachers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        teachers = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        teachers = paginator.page(paginator.num_pages)
    context = {
        'teachers': teachers,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/registration/teachers.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def student_paid_moneys(request, s_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    student = get_object_or_404(Student, user=user_s)
    paid_moneys = PaidMoney.objects.filter(student=student)
    # return HttpResponse(user_s.id)

    context = {
        'paid_moneys': paid_moneys,
        'student': student,
    }
    return render_to_response('userprofile/registration/student_paid_money_list.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def degrees_list(request, s_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    student = get_object_or_404(Student, user=user_s)
    degrees = Degree.objects.filter(student=student)
    # return HttpResponse(user_s.id)

    context = {
        'degrees': degrees,
        'student': student,
    }
    return render_to_response('userprofile/registration/degree_list.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def student_money_paid(request, s_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    student = get_object_or_404(Student, user=user_s)
    form = PaidMoneyForm(request.POST or None, request.FILES or None)
    context = {
        'form': form
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            money = clean_data.get('money')
            paid_date = clean_data.get('paid_date')
            # date = timezone.datetime.strptime(paid_date, "%D-%MM-%YYYY HH:[ss]")
            paid = clean_data.get('paid')
            # return HttpResponse(money)
            paidMoney = PaidMoney(
                student=student,
                money=money,
                paid_date=paid_date,
                paid=paid,
                # paid_date=paid_date,
            )

            paidMoney.save()
            return HttpResponseRedirect('/profile/list/student/{}/paid-moneys/?create-messages='.format(user_s.id) + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')
        else:
            HttpResponse('sds')
    return render_to_response('userprofile/registration/add_or_edit_paid_money.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def degree_create(request, s_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    student = get_object_or_404(Student, user=user_s)
    form = DegreeForm(request.POST or None, request.FILES or None)
    context = {
        'form': form
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            competition = clean_data.get('competition')
            degree = clean_data.get('degree')
            description = clean_data.get('description')
            date = clean_data.get('date')
            degree_c = Degree(
                student=student,
                competition=competition,
                degree=degree,
                description=description,
                date=date,
            )

            degree_c.save()
            return HttpResponseRedirect('/profile/list/student/{}/degrees/?create-messages='.format(user_s.id) + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/add_or_edit_degree.html', context,
                              context_instance=RequestContext(request))



@login_required(login_url='login')
def student_money_paid_edit(request, s_id,mp_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    student = get_object_or_404(Student, user=user_s)
    paid_money_o = get_object_or_404(PaidMoney,student=student,id=mp_id)
    # return HttpResponse(paid_money.money)
    form = PaidMoneyForm(request.POST or None, request.FILES or None,
                         initial={
                             'money': paid_money_o.money,
                             'paid_date': paid_money_o.paid_date,
                             'paid': paid_money_o.paid,
                         },
                         )
    context = {
        'form': form,
        'paid_money': paid_money_o,
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            money = clean_data.get('money')

            # return HttpResponse(money )
            paid_date = clean_data.get('paid_date')
            # date = timezone.datetime.strptime(paid_date, "%D-%MM-%YYYY HH:[ss]")
            paid_form_field = clean_data.get('paid')
            # money_paid = clean_data.get('paid_money_hidden')
            money_paid = request.POST.get('paid_money_hidden')
            # mon = str(money)
            # return HttpResponse(paid_date)
            paid_money_o.money=money
            # paid_date = paid_date.split('/', )
            # paid_date = str(paid_date[2]) + '-' + str(paid_date[0]) + '-' + str(paid_date[1])
            # paid_date = timezone.datetime.strptime(paid_date, "%Y-%m-%d")
            # paid_money_o.paid_date=paid_date,
            # return HttpResponse(money_paid)
            if int(money_paid) == 1:
                paid_tf = True
                # return HttpResponse(paid_tf)
            elif int(money_paid) == 0:
                paid_tf = False
                # return HttpResponse(paid_tf)
            else:
                raise Http404
            paid_tf = bool(paid_tf)
            paid_money_o.paid = paid_tf
            paid_money_o.paid_date = paid_date

            paid_money_o.save()
            # return HttpResponse(paid_money.paid)
            return HttpResponseRedirect('/profile/list/student/{}/paid-moneys/?create-messages='.format(user_s.id) + 'Succesfuly Edited')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/add_or_edit_paid_money.html', context,
                              context_instance=RequestContext(request))



@login_required(login_url='login')
def degree_edit(request, s_id,md_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    student = get_object_or_404(Student, user=user_s)
    degree = get_object_or_404(Degree,student=student,id=md_id)
    form = DegreeForm(request.POST or None, request.FILES or None,
                         initial={
                             'competition': degree.competition,
                             'degree': degree.degree,
                             'description': degree.description,
                             'date': degree.date,
                         },
                         )
    context = {
        'form': form
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            competition = clean_data.get('competition')
            degree_p = clean_data.get('degree')
            description = clean_data.get('description')
            date = clean_data.get('date')
            # date = timezone.datetime.strptime(paid_date, "%D-%MM-%YYYY HH:[ss]")
            #
            # return HttpResponse(competition.length())
            dsk = competition
            degree.competition=competition
            degree.description=description
            degree.description=degree_p
            degree.date=date
            degree.save()
            return HttpResponseRedirect('/profile/list/student/{}/degrees/?create-messages='.format(user_s.id) + 'Succesfuly Edited')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/add_or_edit_degree.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def createteacher(request):
    # user = get_object_or_404(User,id=id)
    # teacher = get_object_or_404(Student,user=user)
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    form = TeacherForm(request.POST or None, request.FILES or None)
    context = {
        'form': form
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            permissions = clean_data.get('permissions')
            first_name = clean_data.get('first_name')
            last_name = clean_data.get('last_name')
            father_name = clean_data.get('father_name')
            passport_number = clean_data.get('passport_number')
            email = clean_data.get('email')
            image = clean_data.get('image')
            birth_date = clean_data.get('birth_date')
            started_date = clean_data.get('started_date')
            home_address = clean_data.get('home_address')
            phone = clean_data.get('phone')
            times_code = clean_data.get('times_code')
            school = clean_data.get('school')
            check = User.objects.all()

            if check.filter(username=passport_number):
                form.add_error('passport_number', passport_number + ' allready exists')
                return render_to_response('userprofile/registration/teacher_add_edit.html', context,
                                          context_instance=RequestContext(request))
            if check.filter(email=email):
                form.add_error('email', email + ' allready exists')
                return render_to_response('userprofile/registration/teacher_add_edit.html', context,
                                          context_instance=RequestContext(request))

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(phone, salt=salt)
            new_user = User(first_name=first_name, last_name=last_name, username=passport_number, email=email,
                            password=password, is_active=True, is_staff=True, is_superuser=False)
            new_user.save()

            teacher = Teacher(
                user=new_user,
                first_name=first_name,
                last_name=last_name,
                father_name=father_name,
                passport_number=passport_number,
                image=image,
                email=email,
                birth_date=birth_date,
                started_date=started_date,
                home_address=home_address,
                phone=phone,
                times_code=times_code,
                school=school,
            )

            teacher.save()
            a_groups = A_Group.objects.filter(pk__in=permissions)
            for a_group in a_groups:
                new_user.groups.add(a_group)
            new_user.save()
            return HttpResponseRedirect('/profile/list/teacher/?create-messages=' + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/teacher_add_edit.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url='login')
def editteacher(request, s_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    user_s = get_object_or_404(User, id=s_id)
    teacher_o = get_object_or_404(Teacher, user=user_s)
    # return HttpResponse('ds')
    # return HttpResponse('dsd')

    a_group_list = []
    for a_group_t in user_s.groups.all():
        a_group_list.append(a_group_t.id)
    form = TeacherForm(request.POST or None, request.FILES or None,
                       initial={
                           'permissions': a_group_list,
                           'first_name': teacher_o.first_name,
                           'last_name': teacher_o.last_name,
                           'father_name': teacher_o.father_name,
                           'passport_number': teacher_o.passport_number,
                           'email': teacher_o.email,
                           'birth_date': teacher_o.birth_date,
                           'started_date': teacher_o.started_date,
                           'home_address': teacher_o.home_address,
                           'phone': teacher_o.phone,
                           'times_code': teacher_o.times_code,
                           'school': teacher_o.school,
                       },
                       )
    context = {
        'form': form,
        'teacher': teacher_o,
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            first_name = clean_data.get('first_name')
            last_name = clean_data.get('last_name')
            father_name = clean_data.get('father_name')
            passport_number = clean_data.get('passport_number')
            permissions = clean_data.get('permissions')
            email = clean_data.get('email')
            image = clean_data.get('image')
            birth_date = clean_data.get('birth_date')
            started_date = clean_data.get('started_date')
            home_address = clean_data.get('home_address')
            phone = clean_data.get('phone')
            times_code = clean_data.get('times_code')
            school = clean_data.get('school')
            check = User.objects.all().exclude(id=s_id)
            if check.filter(username=passport_number):
                form.add_error('passport_number', passport_number + ' allready exists')
                return render_to_response('userprofile/registration/teacher_add_edit.html', context,
                                          context_instance=RequestContext(request))
            if check.filter(email=email):
                form.add_error('email', email + ' allready exists')
                return render_to_response('userprofile/registration/teacher_add_edit.html', context,
                                          context_instance=RequestContext(request))

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(phone, salt=salt)
            # new_user = User(first_name=first_name, last_name=last_name, username=passport_number, email=email,
            #                 password=password, is_active=True,is_staff=True)
            # new_user.save()
            user_s.first_name = first_name
            user_s.last_name = last_name
            user_s.username = passport_number
            user_s.email = email
            user_s.password = password
            user_s.save()

            teacher_o.user = user_s
            teacher_o.first_name = first_name
            teacher_o.last_name = last_name
            teacher_o.email = email
            teacher_o.father_name = father_name
            teacher_o.passport_number = passport_number
            teacher_o.birth_date = birth_date
            teacher_o.started_date = started_date
            if image:
                teacher_o.image = image
            teacher_o.phone = phone
            teacher_o.home_address = home_address
            teacher_o.times_code = times_code
            teacher_o.school = school
            # HttpResponse(teacher_o.first_name)
            # olda_f = teacher_o.father_name
            teacher_o.save()
            a_groups = A_Group.objects.filter(pk__in=permissions)
            user_s.groups.clear()
            if a_groups:
                for a_group in a_groups:
                    user_s.groups.add(a_group)
                user_s.save()
            # return HttpResponse(olda_f + '----------------------------'+teacher_o.father_name + '________________________' +father_name)
            # HttpResponse(teacher_o.first_name)
            # HttpResponse(first_name)
            # teacher = Student(
            #     first_name=first_name,
            #     last_name=last_name,
            #     father_name=father_name,
            #     passport_number=passport_number,
            #     email=email,
            #     birth_date=now,
            #     started_date=now,
            #     home_address=home_address,
            #     phone=phone,
            #     times_code=times_code,
            #     school=school,
            # )
            return HttpResponseRedirect('/profile/list/teacher/?create-messages=' + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/teacher_add_edit.html', context,
                              context_instance=RequestContext(request))



@login_required(login_url='login')
def grouplist(request):
    user = request.user
    if not user.is_superuser:
        raise Http404
    data_list = Group.objects.all()
    paginator = Paginator(data_list, 20)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        groups = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        groups = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        groups = paginator.page(paginator.num_pages)
    context = {
        'groups': groups,
        # 'last_news':last_news
    }

    return render_to_response('userprofile/registration/group_list.html', context,
                              context_instance=RequestContext(request))

@login_required(login_url='login')
def creategroup(request):
    # user = get_object_or_404(User,id=id)
    # student = get_object_or_404(Student,user=user)
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    form = GroupForm(request.POST or None, request.FILES or None)
    context = {
        'form': form
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # return HttpResponse(form.cleaned_data.get('categorys'))
            name = clean_data.get('name')
            series_number = clean_data.get('series_number')
            teacher = clean_data.get('teacher')
            students = clean_data.get('students')
            start_date = clean_data.get('start_date')
            end_date = clean_data.get('end_date')
            # categorys_list = []
            # for news_c_category in news_c.categorys.all():
            #     categorys_list.append(news_c_category.id)
            check = Group.objects.all()
            o_students = Student.objects.filter(id__in=students)
            teacher_o = Teacher.objects.filter(id=teacher).first()
            if check.filter(series_number=series_number):
                form.add_error('series_number', series_number + ' allready exists')
                return render_to_response('userprofile/registration/group_edit_add.html', context,
                                          context_instance=RequestContext(request))
            if o_students.count() > 12:
                form.add_error('students', '12 more student group')
                return render_to_response('userprofile/registration/group_edit_add.html', context,
                                          context_instance=RequestContext(request))
            if o_students:
                for o_student in o_students:
                    for group in Group.objects.filter():
                        if o_student in group.students.all():
                            form.add_error('students', o_student.first_name + o_student.last_name + ' allready exists other group')
                            return render_to_response('userprofile/registration/group_edit_add.html', context,
                                          context_instance=RequestContext(request))
            group = Group(name=name,series_number=series_number,teacher=teacher_o,start_date=start_date,end_date=end_date)
            group.save()
            for o_student in o_students:
                group.students.add(o_student)
            group.save()
            return HttpResponseRedirect('/profile/list/group/?create-messages=' + 'Succesfuly Added')
            # else:
            #     return HttpResponse('dsd')

    return render_to_response('userprofile/registration/group_edit_add.html', context,
                              context_instance=RequestContext(request))




@login_required(login_url='login')
def editgroup(request, g_id):
    now = timezone.now()
    user = request.user
    if not user.is_superuser:
        raise Http404
    group_o = get_object_or_404(Group, id=g_id)
    # return HttpResponse(user_s.id)
    # return HttpResponse('dsd')
    students_list = []
    for group_student in group_o.students.all():
        students_list.append(group_student.id)
    form = GroupForm(request.POST or None, request.FILES or None,
                       initial={
                           'name': group_o.name,
                           'series_number': group_o.series_number,
                           'teacher': group_o.teacher,
                           'students': students_list,
                           'start_date': group_o.start_date,
                           'end_date': group_o.end_date,
                       },
                       )
    context = {
        'form': form,
        'group': group_o,
    }
    if request.method == 'POST':
        if form.is_valid():
            clean_data = form.cleaned_data
            name = clean_data.get('name')
            series_number = clean_data.get('series_number')
            teacher = clean_data.get('teacher')
            students = clean_data.get('students')
            start_date = clean_data.get('start_date')
            end_date = clean_data.get('end_date')



            check = Group.objects.all().exclude(id=g_id)
            o_students = Student.objects.filter(id__in=students)
            teacher_o = Teacher.objects.filter(id=teacher).first()
            if check.filter(series_number=series_number):
                form.add_error('series_number', series_number + ' allready exists')
                return render_to_response('userprofile/registration/group_edit_add.html', context,
                                          context_instance=RequestContext(request))
            if o_students.count() > 12:
                form.add_error('students', '12 more student group')
                return render_to_response('userprofile/registration/group_edit_add.html', context,
                                          context_instance=RequestContext(request))
            if o_students:
                for o_student in o_students:
                    for group in Group.objects.filter().exclude(id=g_id):
                        if o_student in group.students.all():
                            form.add_error('students',
                                           o_student.first_name + o_student.last_name + ' allready exists other group')
                            return render_to_response('userprofile/registration/group_edit_add.html', context,
                                                      context_instance=RequestContext(request))

            group_o.name = name
            group_o.series_number = series_number
            group_o.teacher = teacher_o
            group_o.start_date = start_date
            group_o.end_date = end_date
            for group_student in group_o.students.all():
                group_o.students.remove(group_student)
            for o_student in o_students:
                group_o.students.add(o_student)
            group_o.save()
            return HttpResponseRedirect('/profile/list/group/?create-messages=' + 'Succesfuly Edited')

    return render_to_response('userprofile/registration/group_edit_add.html', context,
                              context_instance=RequestContext(request))