from django.conf.urls import url, include
# from django.contrib.auth import logout

from userprofile.views import *

urlpatterns = [
    url(r'^profile/$', profile, name='profile'),
    url(r'^profile/list/group/$', grouplist, name='grouplist'),
    url(r'^profile/create/group/$', creategroup, name='creategroup'),
    url(r'^profile/edit/group/(?P<g_id>\d+)/$', editgroup, name='editgroup'),
    url(r'^profile/list/student/$', studentlist, name='studentlist'),
    url(r'^profile/edit/student/(?P<s_id>\d+)/$', editstudent, name='editstudent'),
    url(r'^profile/create/student/$', createstudent, name='createstudent'),
    url(r'^profile/list/student/(?P<s_id>\d+)/paid-moneys/$', student_paid_moneys, name='student_paid_moneys'),
    url(r'^profile/list/student/(?P<s_id>\d+)/degrees/$', degrees_list, name='degrees_list'),
    url(r'^profile/student/(?P<s_id>\d+)/money-paid/$', student_money_paid, name='student_money_paid'),
    url(r'^profile/student/(?P<s_id>\d+)/degree-create/$', degree_create, name='degree_create'),
    url(r'^profile/student/(?P<s_id>\d+)/money-paid/(?P<mp_id>\d+)/$', student_money_paid_edit, name='student_money_paid_edit'),
    url(r'^profile/student/(?P<s_id>\d+)/money-degree/(?P<md_id>\d+)/$', degree_edit, name='degree_edit'),
    url(r'^profile/list/teacher/$', teacherlist, name='teacherlist'),
    url(r'^profile/edit/teacher/(?P<s_id>\d+)/$', editteacher, name='editteacher'),
    url(r'^profile/create/teacher/$', createteacher, name='createteacher'),
    url(r'^profile/student/$', profile_student, name='profile_student'),
    url(r'^profile/feedback/$', student_feedback, name='student_feedback'),
    url(r'^profile/feedback/ajax/$', student_feedback_ajax, name='student_feedback_ajax'),
    url(r'^profile/search/$', student_search, name='student_search'),
    url(r'^profile/student/(?P<username>[\w-]+)/details/$', student_view, name='student_view'),
    url(r'^profile/journals/$', journals, name='journals'),
    url(r'^profile/journals/(?P<j_id>\d+)/page/(?P<jp_number>\d+)/$', journal_page, name='journal_page'),
    url(r'^profile/groups/g/(?P<g_id>\d+)/$', group_page, name='group_page'),
    url(r'^profile/groups/s/g/$', my_group, name='my_group'),
    url(r'^profile/journals/ajax/(?P<j_id>\d+)/page/(?P<jp_number>\d+)/$', journal_ajax, name='journal_ajax'),

    # url(r'^settings/change-profiles-info/$', profiles_info_change, name='profiles_info'),
    # url(r'^settings/change-profiles-ajax/$', change_profile_ajax, name='change_profile_ajax'),
    # url(r'^settings/reset-password/$', change_password, name='change_password'),
    # url(r'^change/password/ajax1/$', changePassword_ajax, name='changePassword_ajax'),
    url(r'^logout/$', logout_page,name='logout'),
    url(r'^login/$', login,name='login'),
    url(r'^login/user/$', login_user, name='login_user'),
]