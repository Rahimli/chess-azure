# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-10-17 10:15
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0011_auto_20171017_1352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 17, 10, 15, 41, 966600, tzinfo=utc)),
        ),
    ]
