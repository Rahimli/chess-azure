from ckeditor_uploader.fields import RichTextUploadingField

# from .models import *
from content.models import *
from userprofile.models import *
from django.contrib.auth.models import Group as A_Group
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
my_default_errors = {
    'required': 'This field is required',
    'invalid': 'Enter a valid value'
}


class StudentForm(forms.Form):
    # image = forms.ImageField(required=True)
    first_name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    last_name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    father_name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    image = forms.ImageField(required=False)
    passport_number = forms.CharField(min_length=7,max_length=255,required=True,error_messages=my_default_errors)
    email = forms.EmailField(required=True)
    birth_date = forms.DateField(required=True)
    started_date = forms.DateField(required=True)
    # is_active = forms.BooleanField(label='is Publish')
    # show_slider = forms.BooleanField(label='Unique Name')
    home_address = forms.CharField(max_length=255,required=True)
    phone = forms.CharField(min_length=7,max_length=255,required=True)
    times_code = forms.CharField(min_length=5,max_length=255,required=True)
    school = forms.CharField(min_length=7,max_length=255,required=True)
    def __init__(self,*args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
    # def clean(self):
    #     cleaned_data = self.cleaned_data
    #     # unique_name = cleaned_data.get('unique_name')
    #     # identified = cleaned_data.get('identified_data')
    #     # print(identified)
    #     # if News.objects.filter(unique_name=unique_name).exists() and identified:
    #     #     self._errors['unique_name'] = unique_name +' '+ ' allready exists'
    #
    #     return cleaned_data


class TeacherForm(forms.Form):
    # image = forms.ImageField(required=True)
    permissions = forms.MultipleChoiceField(choices = [], required=False)
    first_name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    last_name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    father_name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    image = forms.ImageField(required=False)
    passport_number = forms.CharField(min_length=7,max_length=255,required=True,error_messages=my_default_errors)
    email = forms.EmailField(required=True)
    birth_date = forms.DateField(required=True)
    started_date = forms.DateField(required=True)
    # is_active = forms.BooleanField(label='is Publish')
    # show_slider = forms.BooleanField(label='Unique Name')
    home_address = forms.CharField(max_length=255,required=True)
    phone = forms.CharField(min_length=7,max_length=255,required=True)
    times_code = forms.CharField(min_length=5,max_length=255,required=True)
    school = forms.CharField(min_length=7,max_length=255,required=True)
    def __init__(self,*args, **kwargs):
        super(TeacherForm, self).__init__(*args, **kwargs)

        a_groups = A_Group.objects.filter()
        self.fields['permissions'].choices = [(x.pk, x.name) for x in
                                              a_groups]

        # def clean(self):
    #     cleaned_data = self.cleaned_data
    #     # unique_name = cleaned_data.get('unique_name')
    #     # identified = cleaned_data.get('identified_data')
    #     # print(identified)
    #     # if News.objects.filter(unique_name=unique_name).exists() and identified:
    #     #     self._errors['unique_name'] = unique_name +' '+ ' allready exists'
    #
    #     return cleaned_data

class PaidMoneyForm(forms.Form):
    # image = forms.ImageField(required=True)
    money = forms.DecimalField(max_digits=19, decimal_places=2,required=True,error_messages=my_default_errors)
    paid_date = forms.DateField(required=True,error_messages=my_default_errors)
    paid = forms.BooleanField(required=False,error_messages=my_default_errors)
    def __init__(self,*args, **kwargs):
        super(PaidMoneyForm, self).__init__(*args, **kwargs)


class DegreeForm(forms.Form):
    # image = forms.ImageField(required=True)
    competition = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    degree = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    description = forms.CharField(widget=forms.Textarea,max_length=255,required=True,error_messages=my_default_errors)
    date = forms.DateField(required=False,error_messages=my_default_errors)
    def __init__(self,*args, **kwargs):
        super(DegreeForm, self).__init__(*args, **kwargs)


class GroupForm(forms.Form):
    # image = forms.ImageField(required=True)
    name = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    series_number = forms.CharField(max_length=255,required=True,error_messages=my_default_errors)
    teacher = forms.ChoiceField(choices = [], required=True)
    students = forms.MultipleChoiceField(choices = [], required=False)
    start_date = forms.DateField(required=False,error_messages=my_default_errors)
    end_date = forms.DateField(required=False,error_messages=my_default_errors)
    def __init__(self,*args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        teachers = Teacher.objects.filter()
        students = Student.objects.filter()
        # for student in students:
        #     for group in Group.objects.filter():
        #         if student in group.students.all() :
        #             students.exclude(id=student.id)



        self.fields['teacher'].choices = [(x.pk, x.first_name +' '+ x.last_name +' '+ x.passport_number) for x in teachers]
        self.fields['students'].choices = [(x.pk, x.first_name +' '+ x.last_name +' '+ x.passport_number) for x in students]
