# from django.conf.urls import url
# from content.views import StudentCreate, StudentUpdate, StudentDelete
#
# urlpatterns = [
#     # ...
#     url(r'student/add/$', StudentCreate.as_view(), name='student-add'),
#     url(r'student/(?P<pk>[0-9]+)/$', StudentUpdate.as_view(), name='student-update'),
#     url(r'student/(?P<pk>[0-9]+)/delete/$', StudentDelete.as_view(), name='student-delete'),
# ]