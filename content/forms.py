from django import forms

from content.models import *


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = [
            'first_name',
            'last_name',
            'father_name',
            'passport_number',
            'birth_date',
            'degree',
            'home_address',
            'phones',
            'email',
            'started_date',
            'times_code',
            'school',
            'end_date',
        ]