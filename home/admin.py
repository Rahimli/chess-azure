from django.contrib import admin

# Register your models here.
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe

from home.models import *


# class ImageWidget(object):
#     pass


class ImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" height="150" title="%s"/></a> '
                           % (value.url, value.url, value.name)))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))



class SliderImageInlineAdmin(admin.TabularInline):
    model = SliderImage
    extra = 1
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            request = kwargs.pop("request", None)
            kwargs['widget'] = ImageWidget
            return db_field.formfield(**kwargs)
        return super(SliderImageInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra

    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     if db_field.name == 'image':
    #         request = kwargs.pop("request", None)
    #         kwargs['widget'] = ImageWidget
    #         return db_field.formfield(**kwargs)
    #     return super(SliderImageInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)

class SliderAdmin(admin.ModelAdmin):
    list_display = ("title", "active","date")
    search_fields = ("title",)
    list_filter = ("title","active",)
    inlines = [SliderImageInlineAdmin, ]

admin.site.register(Slider,SliderAdmin)


class ApplyAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name")
    # inlines = [LessonInline]

admin.site.register(Apply, ApplyAdmin)
admin.site.register(Social)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "order_index")
    prepopulated_fields = {'slug': ("name",)}
    # inlines = [LessonInline]

admin.site.register(Category, CategoryAdmin)


class BlogAdmin(admin.ModelAdmin):
    list_display = ("title", "active")
    prepopulated_fields = {'slug': ("title",)}
    # inlines = [LessonInline]

admin.site.register(Blog, BlogAdmin)

class TournamentAdmin(admin.ModelAdmin):
    list_display = ("title", "active")
    prepopulated_fields = {'slug': ("title",)}
    # inlines = [LessonInline]

admin.site.register(Tournament, TournamentAdmin)



#------------------------------------------------------------------------

class VugarPagePhotoInlineAdmin(admin.TabularInline):
    model = VugarPagePhoto
    extra = 1
    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     if db_field.name == 'photo':
    #         request = kwargs.pop("request", None)
    #         kwargs['widget'] = ImageWidget
    #         return db_field.formfield(**kwargs)
    #     return super(VugarPagePhotoInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra

class VugarPageMuseumPhotoInlineAdmin(admin.TabularInline):
    model = VugarPageMuseumPhoto
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


class VugarChildhoodPhotoInlineAdmin(admin.TabularInline):
    model = VugarChildhoodPhoto
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


# class VugarGeniusPhotoInlineAdmin(admin.TabularInline):
#     model = VugarGeniusPhoto
#     extra = 1
#
#     def get_extra(self, request, obj=None, **kwargs):
#         if obj:
#             return 1
#         return self.extra


# class VugarParticipanPhotoInlineAdmin(admin.TabularInline):
#     model = VugarParticipanPhoto
#     extra = 1
#
#     def get_extra(self, request, obj=None, **kwargs):
#         if obj:
#             return 1
#         return self.extra


class VugarDisinterestedHumanPhotoInlineAdmin(admin.TabularInline):
    model = VugarDisinterestedHumanPhoto
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


class VugarPageAdmin(admin.ModelAdmin):
    list_display = ("full_name",)
    inlines = [VugarPageMuseumPhotoInlineAdmin,
               VugarPagePhotoInlineAdmin,
               VugarChildhoodPhotoInlineAdmin,
               # VugarGeniusPhotoInlineAdmin,
               # VugarParticipanPhotoInlineAdmin,
               VugarDisinterestedHumanPhotoInlineAdmin,
               ]
admin.site.register(VugarPage, VugarPageAdmin)

#------------------------------------------------------------------------