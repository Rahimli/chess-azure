from django.contrib.auth.models import User
from django.db import models
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
# Create your models here.
from django.utils import timezone
from chess import settings
from django.core.validators import *

class Teacher(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,blank=True,null=True,)
    first_name = models.CharField(max_length=255,blank=True,null=True)
    last_name = models.CharField(max_length=255,blank=True,null=True)
    father_name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='teacher/photos',blank=True,null=True)
    passport_number = models.CharField(max_length=255,unique=True, validators=[MinLengthValidator(7)])
    email = models.EmailField()
    birth_date = models.DateField()
    degree  = models.CharField(max_length=255)
    home_address = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    started_date = models.DateField()
    times_code = models.CharField(max_length=255)
    school = models.CharField(max_length=255)
    end_date = models.DateTimeField(blank=True,null=True)
    # def clean_field(self):
    #     cd = self.cleaned_data
    #     pk = self.instance.pk
    #     insert = pk == None
    #     user_e = User.objects.filter(email=self.email).first()
    #     if insert:
    #         raise forms.ValidationError('Some error message!')
    #     else:
    #         pass
    #     return field
    # def clean(self):
    #     cd = self.cleaned_data
    #     user_e = User.objects.filter(email=self.email).first()
    #     if user_e:
    #         self.add_error('email', "passwords do not match !")
    #     return cd
    class Meta:
        verbose_name_plural = "Trainers"
        verbose_name = "Trainer"
    def __str__(self):
        return '{} {} '.format(self.first_name, self.last_name)

    def get_groups(self):
        groups = Group.objects.filter(teacher=self).order_by('-start_date')
        return groups

def student_upload_location(instance,filename):
    return "student/%s/%s" %(instance.passport_number,filename)
class Student(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,blank=True,null=True,editable=False)
    first_name = models.CharField(max_length=255,blank=True,null=True)
    last_name = models.CharField(max_length=255,blank=True,null=True)
    father_name = models.CharField(max_length=255)
    image = models.ImageField(upload_to=student_upload_location,blank=True,null=True)
    passport_number = models.CharField(max_length=255,unique=True, validators=[MinLengthValidator(7)])
    email = models.EmailField()
    birth_date = models.DateField()
    home_address = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, validators=[MinLengthValidator(7)])
    started_date = models.DateField()
    times_code = models.CharField(max_length=255)
    school = models.CharField(max_length=255)
    lesson_count = models.IntegerField(default=0)
    end_date = models.DateTimeField(blank=True,null=True)


    def __str__(self):
        return '{} {} - {}'.format(self.user.first_name,self.user.last_name,self.father_name,self.times_code)

    def get_degrees(self):
        degrees = Degree.objects.filter(student=self).order_by('-date')
        return degrees

    def get_paid_moneys(self):
        moneys = PaidMoney.objects.filter(student=self).order_by('-paid_date')
        return moneys

    def get_participants(self):
        participants = Participant.objects.filter(student=self)
        return participants



class PaidMoney(models.Model):
    student = models.ForeignKey('Student')
    money = models.DecimalField(max_digits=19, decimal_places=2)
    paid_date = models.DateField(blank=True,null=True)
    paid = models.BooleanField(default=False)

    def __str__(self):
        return '{} {} - {}'.format(self.student.user.first_name,self.student.user.last_name,self.money)


class Degree(models.Model):
    student = models.ForeignKey('Student')
    competition = models.CharField(max_length=255)
    degree = models.CharField(max_length=255)
    description = models.TextField(blank=True,null=True)
    date = models.DateField(blank=True,null=True)
    def __str__(self):
        return '{} {} - {}'.format(self.student.user.first_name,self.student.user.last_name,self.degree)

class Group(models.Model):
    name = models.CharField(max_length=255)
    series_number = models.CharField(max_length=255,unique=True)
    teacher = models.ForeignKey('Teacher')
    students = models.ManyToManyField('Student',blank=True,null=True)
    start_date = models.DateTimeField(default=timezone.now())
    end_date = models.DateTimeField(blank=True,null=True)
    # def clean(self, *args, **kwargs):
    #     if self.students.count() > 3:
    #         raise ValidationError("You can't assign more than 12 student")
    #     super(Group, self).clean(*args, **kwargs)
        #This will not work cause m2m fields are saved after the model is saved
    def __str__(self):
        return '{} - {}'.format(self.name,self.teacher.first_name + ' ' + self.teacher.last_name)
    def get_journal(self):
        journal = Journal.objects.filter(group=self).first()
        return journal
    # def clean(self):
    #     from django.core.exceptions import ValidationError
    #     if self.students.all():
    #         print(self.students.all())
    #         if self.students.count() > 12:
    #             raise ValidationError('12den artiq telebe olmaz')
    #     for student in self.students.all():
    #         if Group.objects.filter(students=student).exclude(series_number=self.series_number):
    #             raise ValidationError('1 usaq 1 den artiq qrupda ola bilmez')

class Journal(models.Model):
    name = models.CharField(max_length=255)
    group = models.OneToOneField(Group)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return '{}'.format(self.name)
    def get_pages(self):
        pages = JournalPage.objects.filter(journal=self)
        return pages

class JournalPage(models.Model):
    name = models.CharField(max_length=255,blank=True)
    ord = models.IntegerField(blank=False,null=False)
    journal = models.ForeignKey('Journal')
    class Meta:
        ordering = ["ord"]
        unique_together = (('ord','journal'),('name','journal'))

    def __str__(self):
        return '{} {} '.format(self.name,self.ord)
    # def __str__(self):
    #     return self.group.name

class JournalPageDate(models.Model):
    journal_page = models.ForeignKey('JournalPage',blank=True,null=True)
    day = models.IntegerField(default=0)
    date = models.DateField(blank=True,null=True)
    def __str__(self):
        return '{} - {} {}'.format(self.journal_page.name)


class Lesson(models.Model):
    group = models.ForeignKey('Group',blank=True,null=True)
    date = models.DateTimeField(blank=True,null=True)

class Participant(models.Model):
    day = models.IntegerField(blank=True,null=True)
    journalpage = models.ForeignKey('JournalPage')
    student = models.ForeignKey('Student')
    is_here = models.BooleanField()
    date = models.DateField(blank=True,null=True)
    def __str__(self):
        return '{} - {} {}'.format(self.journalpage.name,self.student.user.first_name,self.student.user.last_name)

class Feedback(models.Model):
    student = models.ForeignKey('Student')
    title = models.CharField(max_length=255)
    content = models.TextField()
    date = models.DateField(auto_now_add=True)
    def __str__(self):
        return '{} {} '.format(self.student.user.first_name,self.student.user.last_name,self.title)

class TeacherRate(models.Model):
    student = models.ForeignKey('Student',blank=True,null=True)
    teacher = models.ForeignKey('Teacher',blank=True,null=True)
    rate = models.CharField(max_length=255,blank=True,null=True)
    date = models.DateField(auto_now_add=True)
    def __str__(self):
        return '{} {} '.format(self.teacher.user.first_name,self.teacher.user.last_name,self.rate)

# @receiver(pre_save, sender=LessonProgram)
# def students_lesson(sender,instance,**kwargs):
#     if kwargs['created']:
#         # instance = kwargs['instance']
#         lesson_students = instance.group.students
#         for instance_student in instance.students:
#             po = Participant.objects.filter(lesson=instance.lesson,student=instance_student)
#             if po:
#                 pass
#             else:
#                 p = Participant(lesson=instance.lesson,student=instance_student,is_here=False)
#                 p.save()