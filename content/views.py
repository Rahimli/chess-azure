from django.http import JsonResponse
from django.shortcuts import render



# # Create your views here.
# from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.core.urlresolvers import reverse_lazy
# from content.models import Student
#
# class AjaxableResponseMixin(object):
#     """
#     Mixin to add AJAX support to a form.
#     Must be used with an object-based FormView (e.g. CreateView)
#     """
#     def form_invalid(self, form):
#         response = super(AjaxableResponseMixin, self).form_invalid(form)
#         if self.request.is_ajax():
#             return JsonResponse(form.errors, status=400)
#         else:
#             return response
#
#     def form_valid(self, form):
#         # We make sure to call the parent's form_valid() method because
#         # it might do some processing (in the case of CreateView, it will
#         # call form.save() for example).
#         response = super(AjaxableResponseMixin, self).form_valid(form)
#         if self.request.is_ajax():
#             data = {
#                 'pk': self.object.pk,
#             }
#             return JsonResponse(data)
#         else:
#             return response
#
# class StudentCreate(AjaxableResponseMixin,CreateView):
#     form_class = StudentForm
#     model = Student
#
#     def dispatch(self, *args, **kwargs):
#         self.course = get_object_or_404(Class, pk=kwargs['class_id'])
#         return super(CourseEntryCreateView, self).dispatch(*args, **kwargs)
#
#     def form_valid(self, form):
#         self.object = form.save(commit=False)
#         self.object.course = self.course
#         self.object.save()
#         return HttpResponseRedirect(self.get_success_url())
# class StudentUpdate(UpdateView):
#     model = Student
#     fields = ['name']
#
# class StudentDelete(DeleteView):
#     model = Student
#     success_url = reverse_lazy('author-list')

