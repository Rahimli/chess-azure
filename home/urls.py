from django.conf.urls import url, include
# from django.contrib.auth import logout
from home.views import *
from userprofile.views import *

urlpatterns = [
    url(r'^$', home, name='index'),
    url(r'^apply/$', apply, name='apply'),
    url(r'^blog/$', blog, name='blog'),
    url(r'^news/$', news, name='news'),
    url(r'^tournaments/$', tournaments, name='tournaments'),
    url(r'^blog/tournament/(?P<n_id>\d+)/(?P<n_slug>[\w-]+)$', tournament, name='tournament'),
    url(r'^blog/category/(?P<c_id>\d+)/$', blog_category, name='blog_category'),
    url(r'^blog/news/(?P<n_id>\d+)/(?P<n_slug>[\w-]+)$', blog_news, name='blog_news'),
    url(r'^about-vugar/(?P<v_slug>[\w-]+)$', about_vuqar, name='about_vuqar'),
    ]