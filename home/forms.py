from django import forms
from captcha.fields import ReCaptchaField

from django.utils.translation import ugettext_lazy as _

class ApplyForm(forms.Form):
    first_name = forms.CharField( required=True,widget=forms.TextInput(attrs={'placeholder': _('Your Firstname: *')}))
    last_name = forms.CharField( required=True,widget=forms.TextInput(attrs={'placeholder': _('Your Lastname: *')}))
    phone = forms.CharField( required=True,widget=forms.TextInput(attrs={'placeholder': _('Phone: *')}))
    birth_date = forms.DateField(widget=forms.DateInput(attrs={'placeholder': _('Birth Date: *')}))
    message = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Message: *')}),required=False)
    # captcha = ReCaptchaField()