from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
from django.utils.timezone import datetime


class SiteInformation(models.Model):
    site_name = models.CharField(max_length=255)
    site_url = models.IntegerField()
    # active = models.BooleanField()
    meta_description = models.TextField(blank=True, null=True)
    meta_keywords = models.TextField(blank=True, null=True)
    # slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.site_name

    def get_images(self):
        return SliderImage.objects.filter(slider=self, active=True).order_by('order_index')

class Category(models.Model):
    name = models.CharField(max_length=255)
    order_index = models.IntegerField()
    active = models.BooleanField()
    description = models.TextField(blank=True, null=True)
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
    def get_images(self):
        return SliderImage.objects.filter(slider=self, active=True).order_by('order_index')


class Blog(models.Model):
    category = models.ForeignKey('Category')
    title = models.CharField(max_length=255)
    short_text = models.TextField(max_length=500)
    image = models.ImageField(upload_to='blog/post/')
    active = models.BooleanField()
    content = RichTextUploadingField(config_name='awesome_ckeditor')
    publish_start_date = models.DateTimeField(null=False,default=datetime.now())
    publish_end_date = models.DateTimeField(null=True,blank=True)
    meta_description = models.CharField(max_length=1024,blank=True)
    meta_keywords = models.CharField(max_length=1024,blank=True)
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title


date = models.DateTimeField(auto_now_add=True)


def get_images(self):
    return SliderImage.objects.filter(slider=self, active=True).order_by('order_index')


class Slider(models.Model):
    title = models.CharField(max_length=255)
    active = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

    def get_images(self):
        return SliderImage.objects.filter(slider=self, active=True).order_by('order_index')


class SliderImage(models.Model):
    slider = models.ForeignKey('Slider')
    # title = models.CharField(max_length=255,blank=True,null=True)
    order_index = models.IntegerField()
    # content = models.TextField(blank=True, null=True,editable=True)
    image = models.ImageField(upload_to='slider-image')
    url = models.URLField(blank=True, null=True)
    active = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)


class Apply(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    birth_date = models.DateField(auto_now_add=False, auto_now=False)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    is_read = models.BooleanField(default=False)
    def __str__(self):
        return '{} - {}'.format(self.first_name,self.last_name)


class Tournament(models.Model):
    title = models.CharField(max_length=255)
    short_text = models.TextField(max_length=500)
    image = models.ImageField(upload_to='tournament/post/')
    active = models.BooleanField()
    content = RichTextUploadingField(config_name='awesome_ckeditor')
    publish_start_date = models.DateTimeField(null=False,default=datetime.now())
    tournament_date = models.DateTimeField()
    publish_end_date = models.DateTimeField(null=True,blank=True)
    meta_description = models.CharField(max_length=1024,blank=True)
    meta_keywords = models.CharField(max_length=1024,blank=True)
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

İCON_CHOICES = (
              ('socialicon socialicon-facebook', "Facebook"),
              ('socialicon socialicon-instagram', "İnstagram"),
              ('socialicon socialicon-youtube', "Youtube"),
              ('socialicon socialicon-twitter', "Twitter"),
              ('socialicon socialicon-google', "Google Plus"),
             )

class Social(models.Model):
    name = models.CharField(max_length=255)
    icon_class = models.CharField(max_length=255,choices=İCON_CHOICES)
    order_index = models.IntegerField()
    url = models.URLField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name


class VugarPage(models.Model):
    active = models.BooleanField()
    full_name = models.CharField(max_length=255)
    # short_description = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    # portrait_image = models.ImageField(upload_to='vugar/portrait/',blank=True,)
    biography = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    titles = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    # participan = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    aphorism = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    vuqar_in_the_world = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    genius = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    disinterested_human = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    vugar_museum = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.full_name
    def get_museum_images(self):
        return VugarPageMuseumPhoto.objects.filter(vugar_page=self)
    def get_photos(self):
        return VugarPagePhoto.objects.filter(vugar_page=self)
    def get_childhood_photos(self):
        return VugarChildhoodPhoto.objects.filter(vugar_page=self)
    # def get_genius_photos(self):
    #     return VugarGeniusPhoto.objects.filter(vugar_page=self)
    # def get_participan_photos(self):
    #     return VugarParticipanPhoto.objects.filter(vugar_page=self)
    def get_disinterested_human_photos(self):
        return VugarDisinterestedHumanPhoto.objects.filter(vugar_page=self)

class VugarPagePhoto(models.Model):
    vugar_page = models.ForeignKey('VugarPage')
    short_description = models.CharField(max_length=500,blank=True,null=True)
    photo = models.ImageField(upload_to='vugar/photos/')
    date = models.DateTimeField(auto_now_add=True)

class VugarPageMuseumPhoto(models.Model):
    vugar_page = models.ForeignKey('VugarPage')
    short_description = models.CharField(max_length=500,blank=True,null=True)
    photo = models.ImageField(upload_to='vugar/museum/')
    date = models.DateTimeField(auto_now_add=True)

class VugarChildhoodPhoto(models.Model):
    vugar_page = models.ForeignKey('VugarPage')
    short_description = models.CharField(max_length=500,blank=True,null=True)
    photo = models.ImageField(upload_to='vugar/childhood/')
    date = models.DateTimeField(auto_now_add=True)

# class VugarGeniusPhoto(models.Model):
#     vugar_page = models.ForeignKey('VugarPage')
#     short_description = models.CharField(max_length=500,blank=True,null=True)
#     photo = models.ImageField(upload_to='vugar/genius/')
#     date = models.DateTimeField(auto_now_add=True)

# class VugarParticipanPhoto(models.Model):
#     vugar_page = models.ForeignKey('VugarPage')
#     short_description = models.CharField(max_length=500,blank=True,null=True)
#     photo = models.ImageField(upload_to='vugar/participan/')
#     date = models.DateTimeField(auto_now_add=True)

class VugarDisinterestedHumanPhoto(models.Model):
    vugar_page = models.ForeignKey('VugarPage')
    short_description = models.CharField(max_length=500,blank=True,null=True)
    photo = models.ImageField(upload_to='vugar/disinterested-human/')
    date = models.DateTimeField(auto_now_add=True)