import hashlib
import random

from django.contrib import admin
# from nested_inline.admin import NestedStackedInline, NestedModelAdmin
# Register your models here.
from django.contrib.auth.hashers import make_password

from content.models import *

class TeacherAdmin(admin.ModelAdmin):
    list_display = ("first_name","last_name", 'father_name', 'passport_number', 'birth_date',)
#     def save_model(self, request, instance, form, change):
#         # user = request.user
#         instance = form.save(commit=False)
#         random_string = str(random.random()).encode('utf8')
#         salt = hashlib.sha1(random_string).hexdigest()[:5]
#         password = make_password(instance.phone, salt=salt)
#         user = User.objects.filter(username=instance.passport_number).first()
#         user_e = User.objects.filter(email=instance.email).first()
#         if change:
#             if user is None or instance.user == user:
#                 if user_e or instance.user == user_e:
#                     instance.user.first_name = instance.first_name
#                     instance.user.last_name = instance.last_name
#                     instance.user.username = instance.passport_number
#                     instance.user.password = password
#                     instance.user.email = instance.email
#                     instance.user.save()
#                 else:
#                     form.add_error('phone number is allready use', instance.phone + ' allready exists')
#             # elif user is None:
#             else:
#                 form.add_error('pasport number is allready use', instance.passport_number + ' allready exists')
#         else:
#             if user:
#                 form.add_error('pasport number is allready use',instance.passport_number + ' allready exists')
#             else:
#                 user = User(username=instance.passport_number, password=password, email=instance.email,first_name=instance.first_name,last_name=instance.last_name,is_staff=True,is_superuser=True)
#                 user.save()
#                 instance.user = user
#         # instance.us
#         instance.save()
#         form.save_m2m()
#         return instance
#
admin.site.register(Teacher,TeacherAdmin)

admin.site.register(Feedback)

class PaidMoneyAdmin(admin.ModelAdmin):
    list_display = ("student", "money","paid_date","paid",)
    search_fields = ("student","money",)
    list_filter = ("paid_date","paid",)

admin.site.register(PaidMoney,PaidMoneyAdmin)


# admin.site.register(Student)

# class PaidMoneyInlineAdmin(admin.TabularInline):
#     model = PaidMoney
#     extra = 1
#
#     def get_extra(self, request, obj=None, **kwargs):
#         if obj:
#             return 1
#         return self.extra


class DegreeInlineAdmin(admin.TabularInline):
    model = Degree
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra

        # def formfield_for_dbfield(self, db_field, **kwargs):
        #     if db_field.name == 'image':
        #         request = kwargs.pop("request", None)
        #         kwargs['widget'] = ImageWidget
        #         return db_field.formfield(**kwargs)
        #     return super(NewsImageInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)

#
# class StudentAdmin(admin.ModelAdmin):
#     list_display = ("user", 'father_name', 'passport_number', 'birth_date', 'times_code',)
#     list_display_links = ("user", 'father_name', 'passport_number', 'birth_date', 'times_code',)
#     list_filter = ('user', 'birth_date', 'started_date',)
#     search_fields = ['user', 'father_name', 'passport_number', ]
#     inlines = [PaidMoneyInlineAdmin, DegreeInlineAdmin, ]
#
#     def save_model(self, request, instance, form, change):
#         # user = request.user
#         instance = form.save(commit=False)
#         random_string = str(random.random()).encode('utf8')
#         salt = hashlib.sha1(random_string).hexdigest()[:5]
#         password = make_password(instance.phone, salt=salt)
#         user = User.objects.filter(username=instance.passport_number).first()
#         user_e = User.objects.filter(email=instance.email).first()
#         if change:
#             if user is None or instance.user == user:
#                 if user_e or instance.user == user_e:
#                     instance.user.first_name = instance.first_name
#                     instance.user.last_name = instance.last_name
#                     instance.user.username = instance.passport_number
#                     instance.user.password = password
#                     instance.user.email = instance.email
#                     instance.user.save()
#                 else:
#                     form.add_error('phone number is allready use', instance.phone + ' allready exists')
#             # elif user is None:
#             else:
#                 form.add_error('pasport number is allready use', instance.passport_number + ' allready exists')
#         else:
#             if user:
#                 form.add_error('pasport number is allready use',instance.passport_number + ' allready exists')
#             else:
#                 user = User(username=instance.passport_number, password=password, email=instance.email,first_name=instance.first_name,last_name=instance.last_name,is_staff=True)
#                 user.save()
#                 instance.user = user
#         # instance.us
#         instance.save()
#         form.save_m2m()
#         return instance


# admin.site.register(Student, StudentAdmin)


# class ParticipantInline(NestedStackedInline):
#     model = Participant
#     extra = 1
#     fk_name = 'lesson'
#
# class LessonInline(NestedStackedInline):
#     model = Lesson
#     extra = 1
#     fk_name = 'group'
#     inlines = [ParticipantInline]

# class GroupAdmin(admin.ModelAdmin):
#     list_display = ("name", "teacher")
#     # inlines = [LessonInline]

# admin.site.register(Group, GroupAdmin)
class TeacherRateAdmin(admin.ModelAdmin):
    list_display = ("student", "teacher","rate","date",)
    # inlines = [LessonInline]


admin.site.register(TeacherRate, TeacherRateAdmin)


class JournalPageInlineAdmin(admin.TabularInline):
    model = JournalPage
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


class JournalAdmin(admin.ModelAdmin):
    list_display = ("name", "group")
    inlines = [JournalPageInlineAdmin, ]


admin.site.register(Journal, JournalAdmin)


class ParticipantAdmin(admin.ModelAdmin):
    list_display = ("day", "student", "date", "is_here",)


admin.site.register(Participant, ParticipantAdmin)


#
# class ParticipantInlineAdmin(admin.TabularInline):
#     model = Participant
#     extra = 1
#
#     def get_extra(self, request, obj=None, **kwargs):
#         if obj:
#             return 1
#         return self.extra

# class LessonAdmin(admin.ModelAdmin):
#     list_display = ("name",)
#     list_filter = ('name',)
#     prepopulated_fields = {'slug': ('name',)}
#     inlines = [ParticipantInlineAdmin]
# admin.site.register(Lesson, LessonAdmin)
